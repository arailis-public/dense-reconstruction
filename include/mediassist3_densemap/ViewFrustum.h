#ifndef VIEWFRUSTUM_H
#define VIEWFRUSTUM_H


#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <mediassist3_densemap/cloud_types.h>
#include <ros/ros.h>
#include <tf/tf.h>
#include <Eigen/Dense>
#include <visualization_msgs/Marker.h>
 
//#include <geometry_msgs/TransformStamped.h>

namespace mediassist3_densemap
{
	class ViewFrustum
	{
		public:
			ViewFrustum();
			~ViewFrustum();

			void setCameraPose( tf::Pose camPose );
			tf::Pose getCameraPose() const;

			void setCornerPoints(
					pcl::PointXYZRGB p1, pcl::PointXYZRGB p2,
					pcl::PointXYZRGB p3, pcl::PointXYZRGB p4 );

			void setCamIntrinsics( Eigen::Affine3d camIntrinsics );
			Eigen::Affine3d getCamIntrinsics() const;

			void setOriginalImage( PCL_Cloud::Ptr img ) { this->originalImage = img; }
			PCL_Cloud::Ptr getOriginalImage() const;

			void updateMapSpace();

			void intersect( Lap_Cloud::Ptr cloud, std::vector<std::size_t>* indices, float maxDepth ) const;

			visualization_msgs::Marker getDebugMarker() const;

			bool getDirty() const { return dirty; }

		private:

			// Pose of the camera (in map space)
			tf::Pose camPose;
			Eigen::Vector3d camPos;
			Eigen::Affine3d camIntrinsics;

			// Original camera depth point cloud:
			PCL_Cloud::Ptr originalImage;

			// Positions of the currently visible corner points:
			Eigen::Vector3d corner1_viewSpace;	// top
			Eigen::Vector3d corner2_viewSpace;	// right
			Eigen::Vector3d corner3_viewSpace;	// bottom
			Eigen::Vector3d corner4_viewSpace;	// left

			// Positions of the currently visible corner points:
			Eigen::Vector3d corner1_mapSpace;	// top
			Eigen::Vector3d corner2_mapSpace;	// right
			Eigen::Vector3d corner3_mapSpace;	// bottom
			Eigen::Vector3d corner4_mapSpace;	// left

			// Normals of the sides of the camera's view frustrum
			Eigen::Vector3d normal1_mapSpace;	// top
			Eigen::Vector3d normal2_mapSpace;	// right
			Eigen::Vector3d normal3_mapSpace;	// bottom
			Eigen::Vector3d normal4_mapSpace;	// left

			// Indicates whether the pose or the viewspace corners were updated.
			// If so, the mapSpace values need an update.
			bool dirty;
	};
}

#endif
