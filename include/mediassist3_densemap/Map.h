#ifndef MAP_H
#define MAP_H

#include <string>
#include <deque>
#include <mediassist3_densemap/cloud_types.h>
#include <mediassist3_densemap/ViewFrustum.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseStamped.h>
#include "cv_bridge/cv_bridge.h"

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/random_sample.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_srvs/Trigger.h>

#include <dynamic_reconfigure/server.h>
#include <mediassist3_densemap/DenseMapConfig.h>
#include <mediassist3_densemap/cloud_types.h>
#include <mediassist3_densemap/Map.h>
#include <visualization_msgs/Marker.h>
#include <mediassist3_densemap/ViewFrustum.h>
#include <mediassist3_densemap/Cache.h>
#include <nav_msgs/Path.h>

#include <Eigen/Dense>

namespace mediassist3_densemap
{

class Map
{
	public:
		Map( unsigned int ID, float inputPointsAmount=0.1, int minPixelBrightness=50, int maxPixelBrightness=210, float maxDepth=0.1, float depthRemovalThreshold=0.04 );
		~Map();

		CacheEntry addFrame( ros::Time timestamp, tf::Pose pose_tf, Eigen::Affine3d camIntrinsics,
        PCL_Cloud::Ptr input,
				const sensor_msgs::ImageConstPtr& segmentation_msg );
		void mergeFrame( const ViewFrustum& viewFrustum, PCL_CloudLabeled::Ptr input, float maxDepth );

		void rebuildAfterCacheUpdate( Cache& cache );

		//bool checkTrajectoryOverlap( nav_msgs::PathPtr path );
		//void updateTrajectory( nav_msgs::PathPtr path, unsigned int ID );

		void setCutoff( int left, int top, int right, int bottom );

		sensor_msgs::PointCloud2Ptr getFullMap( float minMergeRatio, int minMergeCount,
				bool publishOnlySegmented,
				float minSegmentationReliability, float smoothingRadius,
        bool highlightViewFrustum=false,
				bool highlightSegmentation=false );

		visualization_msgs::Marker getDebugTrajectory( ros::Time timestamp, std_msgs::ColorRGBA col );

		unsigned int getID() { return mID; }

    void setInputPointsAmount( float a ) { inputPointsAmount=a; }
    void setMinPixelBrightness( int b ) { minPixelBrightness=b; }
    void setMaxPixelBrightness( int b ) { maxPixelBrightness=b; }
    void setMaxPixelDepth( float d ) { maxPixelDepth=d; }
    void setDepthRemovalThresh( float d ) { depthRemovalThresh=d; }

	private:

		unsigned int mID;
    int minPixelBrightness;
    int maxPixelBrightness;

    float maxPixelDepth;
    float inputPointsAmount;
    float depthRemovalThresh;

    int maxPixelCol;
    int maxPixelRow;

		void buildTree();
		void recalculateMap();
		void reset();

    Lap_KdTree::Ptr pclTreePtr;
		Lap_Cloud::Ptr pclCloudPtr;

		float octreeResultion;

		// View Frustrum to determine which points should currently be visible:
		ViewFrustum viewFrustum;

		int cutoffLeft, cutoffTop, cutoffRight, cutoffBottom;
		std::vector<int> rand_indices;
};
}

#endif
