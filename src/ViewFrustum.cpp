#include <mediassist3_densemap/ViewFrustum.h>
#include <pcl/conversions.h>
#include <tf_conversions/tf_eigen.h>
#include <pcl/common/transforms.h>


namespace mediassist3_densemap
{
	ViewFrustum::ViewFrustum() {}
	ViewFrustum::~ViewFrustum() {}

	void ViewFrustum::setCameraPose( tf::Pose pose )
	{
		camPose = pose;
		tf::Vector3 o = this->camPose.getOrigin();
		camPos = Eigen::Vector3d( o.x(), o.y(), o.z() );

		// Map space values are now no longer up to date
		dirty = true;
	}

	tf::Pose ViewFrustum::getCameraPose() const
	{
		return camPose;
	}

	Eigen::Affine3d ViewFrustum::getCamIntrinsics() const
  {
    return camIntrinsics;
  }

	void ViewFrustum::setCornerPoints( 
			pcl::PointXYZRGB p1, pcl::PointXYZRGB p2,
			pcl::PointXYZRGB p3, pcl::PointXYZRGB p4 )
	{
		corner1_viewSpace = Eigen::Vector3d( p1.x, p1.y, p1.z );
		corner2_viewSpace = Eigen::Vector3d( p2.x, p2.y, p2.z );
		corner3_viewSpace = Eigen::Vector3d( p3.x, p3.y, p3.z );
		corner4_viewSpace = Eigen::Vector3d( p4.x, p4.y, p4.z );

		// Map space values are now no longer up to date
		dirty = true;
	}

	void ViewFrustum::setCamIntrinsics( Eigen::Affine3d camIntrinsics )
  {
    this->camIntrinsics = camIntrinsics;
  }

	PCL_Cloud::Ptr ViewFrustum::getOriginalImage() const
	{
		return this->originalImage;
	}


	// Transform the corner points into map space using the current camPose
	void ViewFrustum::updateMapSpace()
	{
		// rotate pointcloud
		tf::Quaternion qz(tf::Vector3(0,0,1),-M_PI*0.5f);
		tf::Quaternion qy(tf::Vector3(0,1,0),M_PI*0.5f);
		tf::Pose pose_tf = camPose;
		pose_tf.setRotation( pose_tf.getRotation()*qy*qz );
		PCL_CloudLabeled::Ptr transCloud(new PCL_CloudLabeled());

		Eigen::Affine3d pose_eigen;
		tf::poseTFToEigen( pose_tf, pose_eigen );

		/*corner1_mapSpace = pcl::transformPoint( c1, pose_eigen );
		corner2_mapSpace = pcl::transformPoint( c2, pose_eigen );
		corner3_mapSpace = pcl::transformPoint( c3, pose_eigen );
		corner4_mapSpace = pcl::transformPoint( c4, pose_eigen );*/
		
		corner1_mapSpace = pose_eigen * corner1_viewSpace;
		corner2_mapSpace = pose_eigen * corner2_viewSpace;
		corner3_mapSpace = pose_eigen * corner3_viewSpace;
		corner4_mapSpace = pose_eigen * corner4_viewSpace;


		Eigen::Vector3d topLeft = corner1_mapSpace - camPos;
		Eigen::Vector3d topRight = corner2_mapSpace - camPos;
		Eigen::Vector3d bottomRight = corner3_mapSpace - camPos;
		Eigen::Vector3d bottomLeft = corner4_mapSpace - camPos;

		normal1_mapSpace = topLeft.cross(topRight);
		normal2_mapSpace = topRight.cross(bottomRight);
		normal3_mapSpace = bottomRight.cross(bottomLeft);
		normal4_mapSpace = bottomLeft.cross(topLeft);

		dirty = false;
	}

	void ViewFrustum::intersect( Lap_Cloud::Ptr cloud, std::vector<std::size_t>* indices, float maxDepth ) const
	{
		// Make sure all normals and positions in map space are up-to-date:
		if( dirty )
			throw std::runtime_error("Before intersecting, call ViewFrustum::updateMapSpace()!");

		float maxDepthSquared = maxDepth*maxDepth;

		for( std::size_t i = 0; i < cloud->size(); i++ )
		{
			LapPointType* p = &cloud->at(i);
			p->in_view_frustrum = 0;

			// Vector from point on the plane (camPos) to the new point:
			Eigen::Vector3d v = Eigen::Vector3d( p->x-camPos.x(),
					p->y-camPos.y(), p->z-camPos.z() );
			// First, check if the point is further away than what was seen in the image. If so, keep the point:
			if( v.squaredNorm() > maxDepthSquared )
				continue;
			float dot1 = v.dot( normal1_mapSpace );
			if( dot1 < 0 )
				continue;
			float dot2 = v.dot( normal2_mapSpace );
			if( dot2 < 0 )
				continue;
			float dot3 = v.dot( normal3_mapSpace );
			if( dot3 < 0 )
				continue;
			float dot4 = v.dot( normal4_mapSpace );
			if( dot4 < 0 )
				continue;

			// If all the dot products are > 0, the point lies inside!
			p->in_view_frustrum = 1;
			indices->push_back( i );

		}
	}

	visualization_msgs::Marker ViewFrustum::getDebugMarker() const
	{
		if( dirty )
			throw std::runtime_error("Before calling getDebugMarker, call ViewFrustum::updateMapSpace()!");

		visualization_msgs::Marker marker;
		marker.header.frame_id = "map";
		marker.header.stamp = ros::Time::now();
		marker.ns = "view_frustrum";
		marker.id = 0;
		marker.frame_locked = true;
		marker.type = visualization_msgs::Marker::LINE_LIST;
		marker.action = visualization_msgs::Marker::ADD;
		marker.pose.position.x = 0;
		marker.pose.position.y = 0;
		marker.pose.position.z = 0;
		marker.pose.orientation.x = 0.0;
		marker.pose.orientation.y = 0.0;
		marker.pose.orientation.z = 0.0;
		marker.pose.orientation.w = 1.0;
		marker.scale.x = 0.0005;
		marker.scale.y = 1;
		marker.scale.z = 1;
		marker.color.a = 0.9;
		marker.color.r = 1.0;
		marker.color.g = 1.0;
		marker.color.b = 1.0;

    Eigen::Vector3d c1_eigen = (corner1_mapSpace - camPos).normalized()*0.1 + camPos;
    Eigen::Vector3d c2_eigen = (corner2_mapSpace - camPos).normalized()*0.1 + camPos;
    Eigen::Vector3d c3_eigen = (corner3_mapSpace - camPos).normalized()*0.1 + camPos;
    Eigen::Vector3d c4_eigen = (corner4_mapSpace - camPos).normalized()*0.1 + camPos;

		geometry_msgs::Point camPoint, c1, c2, c3, c4;
		camPoint.x = camPos.x(); camPoint.y = camPos.y(); camPoint.z = camPos.z();
		c1.x = c1_eigen.x(); c1.y = c1_eigen.y(); c1.z = c1_eigen.z();
		c2.x = c2_eigen.x(); c2.y = c2_eigen.y(); c2.z = c2_eigen.z();
		c3.x = c3_eigen.x(); c3.y = c3_eigen.y(); c3.z = c3_eigen.z();
		c4.x = c4_eigen.x(); c4.y = c4_eigen.y(); c4.z = c4_eigen.z();

		std_msgs::ColorRGBA col1;
		std_msgs::ColorRGBA col2;
		col1.a = 0.35; col1.r = 0.3; col1.g = 0.8; col1.b = 0.3;
		col2.a = 0.35; col2.r = 0.1; col2.g = 0.7; col2.b = 0.1;

		marker.points.push_back( camPoint );
		marker.points.push_back( c1 );
		marker.points.push_back( camPoint );
		marker.points.push_back( c2 );
		marker.points.push_back( camPoint );
		marker.points.push_back( c3 );
		marker.points.push_back( camPoint );
		marker.points.push_back( c4 );

		marker.points.push_back( c1 );
		marker.points.push_back( c2 );
		marker.points.push_back( c2 );
		marker.points.push_back( c3 );
		marker.points.push_back( c3 );
		marker.points.push_back( c4 );
		marker.points.push_back( c4 );
		marker.points.push_back( c1 );

		/*// Top side:
		marker.points.push_back( camPoint );
		marker.points.push_back( c2 );
		marker.points.push_back( c1 );
		for( int i=0; i<3; i++ )
			marker.colors.push_back( col1 );
		// Right side:
		marker.points.push_back( camPoint );
		marker.points.push_back( c3 );
		marker.points.push_back( c2 );
		for( int i=0; i<3; i++ )
			marker.colors.push_back( col2 );
		// Bottom side:
		marker.points.push_back( camPoint );
		marker.points.push_back( c4 );
		marker.points.push_back( c3 );
		for( int i=0; i<3; i++ )
			marker.colors.push_back( col1 );
		// Left side:
		marker.points.push_back( camPoint );
		marker.points.push_back( c1 );
		marker.points.push_back( c4 );
		for( int i=0; i<3; i++ )
			marker.colors.push_back( col2 );*/

		return marker;
	}
}


