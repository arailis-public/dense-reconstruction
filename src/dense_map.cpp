// written by Peter K in May 2019
// starts the dense map

#include "mediassist3_densemap/dense_map_node_unbounded.h"
#include <iostream>
#include <ros/ros.h>

using namespace mediassist3_densemap;

int main(int argc, char **argv)
{
	ros::init(argc, argv, "listener_class");
	ros::NodeHandle nh("");
	ros::NodeHandle private_nh("~");

	DenseMapNode dmn;

	dmn.onInit(nh, private_nh);
	ros::spin();

	return 0;
}
