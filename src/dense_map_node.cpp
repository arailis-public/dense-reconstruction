// written by Peter K in May 2019
// adopted by Micha Pfeiffer
// Dense Map node

#include "mediassist3_densemap/dense_map_node_unbounded.h"
#include <random>

#include <boost/version.hpp>
#if ((BOOST_VERSION / 100) % 1000) >= 53
#include <boost/thread/lock_guard.hpp>
#endif

#include <algorithm>
#include <iostream>

#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/octree/octree_search.h>
#include <pcl/conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/extract_indices.h>


#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>

#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <nav_msgs/Path.h>

#include "cv_bridge/cv_bridge.h"
#include <opencv2/opencv.hpp>

#include <dynamic_reconfigure/server.h>
#include <mediassist3_densemap/DenseMapConfig.h>

#include <cmath>

#include <ctime>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

namespace mediassist3_densemap
{

	// using namespace pcl;
	using namespace geometry_msgs;
	using namespace sensor_msgs;

	DenseMapNode::DenseMapNode() : mostRecentMapID(-1), record(false),
    prevMapPublishTime(ros::Time::now())
	{
		resetAll();
	}

	void DenseMapNode::resetAll()
	{
		for( auto it = maps.begin(); it != maps.end(); it++ )
			delete it->second;
		maps.clear();

		inputCache.clear();
		resetCaches();
	}

	void DenseMapNode::onInit(ros::NodeHandle &nh, ros::NodeHandle &private_nh)
	{
		// Find out if we want to use a segmentation mask:
		useSegmentationMask = false;
		private_nh.getParam("use_segmentation_mask", useSegmentationMask);

		std::cout << "Unbounded Sync Node - Using segmentation mask = " << useSegmentationMask << std::endl;
		std::cout << "Unbounded Sync Node - Using unbounded sync = " << unboundedSync << std::endl;

		// Start listening to new poses:
		sub_camera_pose_ =
			new message_filters::Subscriber<PoseStamped>(
					nh, "camera_pose", 100);

		// Listen to results of our depth map requests:
		sub_point_cloud_ =
			new message_filters::Subscriber<PointCloud2>(
					nh, "points2", 100);

		// Listen to updates in the trajectories, in case
		// the SLAM system has closed a loop, merged maps, or optimized a map:
		sub_trajectories_ =
			new message_filters::Subscriber<Path>(
					nh, "trajectories", 300);		// large queue - don't drop any of these!

		// Listen to input images as well, so that we can find the images from which the
                // pose was originally constructed:
                sub_img_left_ =
                        new message_filters::Subscriber<Image>(
                                        nh, "stereo/left/image_rect_color", 100);
                sub_img_left_info_ =
                        new message_filters::Subscriber<CameraInfo>(
                                        nh, "stereo/left/camera_info", 100);

		// Listen to results of our segmentation requests:
		if( useSegmentationMask )
			sub_mask_image_ =
				new message_filters::Subscriber<Image>(
						nh, "image_segmented", 100);

		// Set up publisher which publishes the full map:
		pub_ = private_nh.advertise<PointCloud2>(
				"densemap", 10);

		// Publisher for debug markers
		pub_debug_frustum_ = private_nh.advertise<visualization_msgs::Marker>(
				"view_frustum", 1);

		pub_debug_trajectories_ = private_nh.advertise<visualization_msgs::MarkerArray>(
				"trajectories", 1);

		// Find out how many pixels we should ignore on each side of the input image:
		nh.param("image_cutoff_top", cutoff_top_, 0);
		nh.param("image_cutoff_bottom", cutoff_bottom_, 0);
		nh.param("image_cutoff_left", cutoff_left_, 0);
		nh.param("image_cutoff_right", cutoff_right_, 0);
		nh.param("input_in_mm", input_in_mm_, false);

		// Get queue size for the time synchronizer:
		int queue_size;
		private_nh.param("queue_size", queue_size, 1000);

		// Set some default values:
		minMergeRatio = 3.0;
		minMergeCount = 1;
		publishOnlySegmented = false;
		minSegmentationReliability = 0.5;
		smoothOutputCloud = false;
		smoothingRadius = 0.05;

		if( useSegmentationMask )
      {
        unbounded_sync_seg_ = new TimeSynchronizer<PoseStamped,PointCloud2,Image,CameraInfo>(
                *sub_camera_pose_, *sub_point_cloud_, *sub_mask_image_, *sub_img_left_info_, queue_size );
        unbounded_sync_seg_->registerCallback( boost::bind(
                        &DenseMapNode::unboundedCallback, this, _1, _2, _3, _4) );
      }

    if( !useSegmentationMask )
      {
        unbounded_sync_ = new TimeSynchronizer<PoseStamped,PointCloud2,CameraInfo>(
		*sub_camera_pose_, *sub_point_cloud_, *sub_img_left_info_, queue_size );
        unbounded_sync_->registerCallback( boost::bind(
                        &DenseMapNode::unboundedCallback, this, _1, _2, _3) );
      }


		sub_trajectories_->registerCallback( &DenseMapNode::newTrajectory, this );

		// Set up dynamic reconfigure callback:
		dynConfigServer = std::shared_ptr<DynConfigServerType>( new DynConfigServerType );
		DynConfigServerType::CallbackType f;
		f = boost::bind(&DenseMapNode::newConfigCallback, this, _1, _2);
		dynConfigServer->setCallback(f);

		// Listen to slam_map_reset calls, so that we can reset our map in this case
		serviceSLAMReset = private_nh.advertiseService("map_reset", &DenseMapNode::resetService, this);
	}

	DenseMapNode::~DenseMapNode()
	{
		resetAll();
		if( sub_img_left_ != NULL )
			delete sub_img_left_;
		if( sub_img_left_info_ != NULL )
			delete sub_img_left_info_;
		if( sub_img_right_ != NULL )
			delete sub_img_right_;
		if( sub_img_right_info_ != NULL )
			delete sub_img_right_info_;
		if( sub_camera_pose_ != NULL )
			delete sub_camera_pose_;
		if( sub_point_cloud_ != NULL )
			delete sub_point_cloud_;
		if( sub_mask_image_ != NULL )
			delete sub_mask_image_;
		if( sub_trajectories_ != NULL )
			delete sub_trajectories_;
	}


	void DenseMapNode::newPointsCallback(
			const PointCloud2ConstPtr& point_cloud_2_msg )
	{
		// Check that the received point cloud's time stamp corresponds to the time of the
		// cached pose:
		ROS_INFO_STREAM( "Received new point cloud" );
		if( cachedPose )
		{
			ROS_INFO_STREAM( "Comparing to cached pose" );
			if( cachedPose->header.stamp == point_cloud_2_msg->header.stamp )
			{
				ROS_INFO_STREAM( "Depth map matches cached data, processing..." );
				cachedPoints = point_cloud_2_msg;
				parseCaches();
			}
		}
	}

	void DenseMapNode::newMaskCallback(
			const ImageConstPtr& segmentation_mask )
	{
		// Check that the received mask's time stamp corresponds to the time of the
		// cached pose:
		ROS_INFO_STREAM( "Received new segmentation" );
		if( cachedPose )
		{
			ROS_INFO_STREAM( "Comparing to cached pose" );
			if( cachedPose->header.stamp == segmentation_mask->header.stamp )
			{
				ROS_INFO_STREAM( "Segmentation mask matches cached data, processing..." );
				cachedMask = segmentation_mask;
				parseCaches();
			}
		}

	}

	void DenseMapNode::unboundedCallback(
                        const PoseStampedConstPtr& pose_msg,
                        const PointCloud2ConstPtr& point_cloud_2_msg,
                        const ImageConstPtr& segmentation_mask,
		       	const CameraInfoConstPtr& left_info_msg	)
        {
					ROS_INFO_STREAM( "received set of data" );
	        ros::Duration dtPublish = ros::Time::now() - prevMapPublishTime;
          if( dtPublish.sec > 4.0 )
					{
  					ROS_INFO_STREAM( "No new map published for a while, re-sending old map" );
  					publishCurrentMap();
					}
	  // Store the intrinsics for further use:
          leftCamIntrinsics = Eigen::Affine3d::Identity();
          double* data = leftCamIntrinsics.data();

          // Copy 4x3 into upper left part of 4x4 matrix and convert between row-major and col-major!
          for( int row = 0; row < 3; row++ )
                for( int col = 0; col < 3; col++ )
                        data[col*4+row] = (double)left_info_msg->P[row*4+col];

	  	

					parseNewData( pose_msg, point_cloud_2_msg, segmentation_mask );
        }

        void DenseMapNode::unboundedCallback(
                        const PoseStampedConstPtr& pose_msg,
                        const PointCloud2ConstPtr& point_cloud_2_msg,
			const CameraInfoConstPtr& left_info_msg)
        {
					ROS_INFO_STREAM( "received set of data" );
          ros::Duration dtPublish = ros::Time::now() - prevMapPublishTime;
          if( dtPublish.sec > 4.0 )
          {
               	ROS_INFO_STREAM( "No new map published for a while, re-sending old map" );
                publishCurrentMap();
          }

	  // Store the intrinsics for further use:
          leftCamIntrinsics = Eigen::Affine3d::Identity();
    	  double* data = leftCamIntrinsics.data();
          
	  // Copy 4x3 into upper left part of 4x4 matrix and convert between row-major and col-major!
    	  for( int row = 0; row < 3; row++ )
        	for( int col = 0; col < 3; col++ )
                 	data[col*4+row] = (double)left_info_msg->P[row*4+col];

	  parseNewData( pose_msg, point_cloud_2_msg);
        }

	void DenseMapNode::parseCaches()
	{
		if( cachedPose && cachedPoints )
		{
			// If we're not using a segmentation, go ahead and use this pose/clous pair:
			if( !useSegmentationMask )
			{
				parseNewData( cachedPose, cachedPoints );
				resetCaches();
			} else {
				if( cachedMask )
				{
					parseNewData( cachedPose, cachedPoints, cachedMask );
					resetCaches();
				}
			}
		}
	}

	void DenseMapNode::resetCaches()
	{
		cachedPose = NULL;
		cachedPoints = NULL;
		cachedMask = NULL;
	}


	void DenseMapNode::parseNewData(
			const PoseStampedConstPtr& pose_msg,
			const PointCloud2ConstPtr& point_cloud_2_msg,
			const ImageConstPtr& segmentation_msg)
	{
		PCL_Cloud::Ptr input_cloud(new PCL_Cloud());
		// convert to pcl
		tf::Pose pose_tf;
		tf::poseMsgToTF(pose_msg->pose, pose_tf);
		fromROSMsg(*point_cloud_2_msg, *input_cloud);

		// correct the scaling of the cloud for daVinci
		if ( input_in_mm_ ) {
			Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
			transform *= 1e-3;
			pcl::transformPointCloud (*input_cloud, *input_cloud, transform);
		}

		// Find the map into which this point cloud belongs:
		// This assumes the pose is in a frame called "mapX", where X is the map ID and must
		// be an unsigned int.
		unsigned int mapID = extractMapID( pose_msg->header.frame_id );

		ROS_INFO_STREAM( "Adding new data to map: " << mapID );

		// Check if a map with this ID already exists, otherwise create it:
		if( maps.count( mapID ) == 0 )
		{
			maps[mapID] = new Map( mapID, inputPointsAmount, minPixelBrightness, maxPixelBrightness, maxPixelDepth, depthRemovalThresh );
		}

		// Now this map must exist, so we can use it:
		Map* curMap = maps[mapID];

		curMap->setCutoff( cutoff_left_, cutoff_top_, cutoff_right_, cutoff_bottom_ );
		CacheEntry cacheEntry = curMap->addFrame(
				pose_msg->header.stamp, pose_tf, leftCamIntrinsics, input_cloud, segmentation_msg );

		inputCache.addEntry( cacheEntry );

		mostRecentMapID = mapID;

		publishCurrentMap();

    if( publishViewFrustum )
		{
			ROS_WARN_STREAM( "Publish view frustum" );
      publishDebugViewFrustrum( cacheEntry.viewFrustum );
		}
	}

	void DenseMapNode::publishDebugViewFrustrum( const ViewFrustum& viewFrustum )
	{
		visualization_msgs::Marker marker = viewFrustum.getDebugMarker();
		pub_debug_frustum_.publish( marker );
	}

	void DenseMapNode::publishCurrentMap()
	{
		ROS_INFO_STREAM( "Publishing map: " << mostRecentMapID << "/" << maps.size() );
		if( mostRecentMapID < 0 || mostRecentMapID >= maps.size() )
			return;

		Map* curMap = maps[mostRecentMapID];

		float sRadius = smoothingRadius;
		if(!smoothOutputCloud)
			sRadius = 0;

		sensor_msgs::PointCloud2Ptr outCloud = curMap->getFullMap(
				minMergeRatio, minMergeCount,
				publishOnlySegmented,
				minSegmentationReliability,
				sRadius,
        highlightViewFrustum,
				highlightSegmentation );

		//outCloud->header.frame_id = pose_msg->header.frame_id;
		outCloud->header.frame_id = "map";

		ROS_INFO_STREAM( "Publishing map: " << outCloud->header.frame_id <<
				"\tNumber of points published: " << outCloud->width*outCloud->height );

		pub_.publish( outCloud );

    prevMapPublishTime = ros::Time::now();
	}


	void DenseMapNode::publishDebugTrajectories( ros::Time timestamp )
	{
		visualization_msgs::MarkerArray markers;

		std::vector<std_msgs::ColorRGBA> colors;
		std_msgs::ColorRGBA col;
		col.r = 0.5; col.g = 0; col.b = 0; col.a = 1;
		colors.push_back( col );
		col.r = 0; col.g = 0.5; col.b = 0; col.a = 1;
		colors.push_back( col );
		col.r = 0; col.g = 0; col.b = 0.5; col.a = 1;
		colors.push_back( col );
		col.r = 0.5; col.g = 0; col.b = 0.5; col.a = 1;
		colors.push_back( col );
		col.r = 0.5; col.g = 0.5; col.b = 0; col.a = 1;
		colors.push_back( col );
		col.r = 0; col.g = 0.5; col.b = 0.5; col.a = 1;
		colors.push_back( col );
		col.r = 0.25; col.g = 0.25; col.b = 0.5; col.a = 1;
		colors.push_back( col );
		col.r = 0.25; col.g = 0.5; col.b = 0.25; col.a = 1;
		colors.push_back( col );
		col.r = 0.5; col.g = 0.25; col.b = 0.25; col.a = 1;
		colors.push_back( col );

		for( auto it = maps.begin(); it != maps.end(); it++ )
		{
			Map* map = it->second;

			unsigned int mapID = map->getID();

			visualization_msgs::Marker marker = inputCache.getDebugTrajectory( timestamp,
					colors[ mapID % colors.size() ], mapID );
			markers.markers.push_back(marker);
		}
		pub_debug_trajectories_.publish( markers );
	}

bool DenseMapNode::resetService( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res )
{
	ROS_INFO_STREAM("Received map reset trigger.");
	resetAll();
	return true;
}

void DenseMapNode::newConfigCallback( DenseMapConfig &config, std::uint32_t level )
{
	ROS_INFO("Reconfigure request");
	record = config.record;
	inputPointsAmount = config.input_points_amount;
	minMergeRatio = config.min_merge_ratio;
	minMergeCount = config.min_merge_count;
	smoothOutputCloud = config.smooth_output_cloud;
	smoothingRadius = config.smoothing_radius;
	publishOnlySegmented = config.publish_only_segmented;
	minSegmentationReliability = config.min_segmentation_reliability;
  minPixelBrightness = config.min_pixel_brightness;
  maxPixelBrightness = config.max_pixel_brightness;
  publishViewFrustum = config.publish_view_frustum;
  highlightViewFrustum = config.highlight_view_frustum;
  highlightSegmentation = config.highlight_segmentation;
  maxPixelDepth = config.max_pixel_depth*1e-3;		// Given in mm, change to m
	depthRemovalThresh = config.depth_removal_threshold*1e-3;		// Given in mm, change to m
	if( publishOnlySegmented && !useSegmentationMask )
	{
		ROS_WARN("Trying to publish only segmented part of point cloud, but mask is not being segmented! Restart node with use_segmentation_mask:=true.");
		publishOnlySegmented = false;
	}
  for( unsigned int i = 0; i < maps.size(); i++ )
  {
    maps[i]->setInputPointsAmount( inputPointsAmount );
    maps[i]->setMinPixelBrightness( minPixelBrightness );
    maps[i]->setMaxPixelBrightness( maxPixelBrightness );
    maps[i]->setMaxPixelDepth( maxPixelDepth );
    maps[i]->setDepthRemovalThresh( depthRemovalThresh );
  }
	publishCurrentMap();
}

/* If the frameID contains mapXY where XY is an integer, return this integer.
 * If there is no such ID in the frame name, return 0 as the default map ID */
unsigned int DenseMapNode::extractMapID( std::string frameID )
{
	// Assume the first 3 letters are "map" and the rest is the ID:
	if( frameID.size() > 3 )
	{
		unsigned int ID = std::stoi( frameID.substr(3) );
		return ID;
	} else {
		return 0;
	}
}

void DenseMapNode::newTrajectory( const PathPtr& path )
{
	unsigned int mapID = extractMapID( path->header.frame_id );
	ROS_INFO_STREAM( "New Path received for map with ID: " << mapID );
	/*for( unsigned int i = 0; i < path->poses.size(); i ++ )
	{
		auto p = path->poses[i].pose;
		ROS_WARN_STREAM( p.position.x << " " << p.position.y << " " << p.position.z );
	}*/

	//maps[i]->updateTrajectory( path, mapID );
	inputCache.updateTrajectory( path, mapID );

	if( maps.count(mapID) == 0 )
	{
		ROS_WARN_STREAM( "Received trajectory update for map " << mapID <<
				", but map with this ID doesn't exist." );
		return;
	}

	Map* curMap = maps[mapID];
	ROS_INFO_STREAM( "Will rebuild map " << mapID );
	ROS_INFO_STREAM( "ID test " << curMap << " " << curMap->getID() );
	curMap->rebuildAfterCacheUpdate( inputCache );


	//Map* curMap = maps[mapID];
	//curMap->updateTrajectory( path );

	if( pub_debug_trajectories_.getNumSubscribers() > 0 )
	{
		publishDebugTrajectories( path->header.stamp );
	}

		// re-publish the updated map:
	/*sensor_msgs::PointCloud2Ptr outCloud = curMap->getFullMap( publishOnlySegmented,
	  minSegmentationReliability, smoothingRadius );
	  outCloud->header.frame_id = path->header.frame_id;
	  pub_.publish( outCloud );*/

}

} 

